---
sidebar_position: 2
---

# Java Microbenchmark Harness


* [Upstream Github Link](https://github.com/openjdk/jmh)

```
JMH is a Java harness for building, running, and analysing
nano/micro/milli/macro benchmarks written in Java and other
languages targeting the JVM.
```

Put another way it's a utility for running tests
that benchmark code. With a similar syntax to unit
tests. It's best for benchmarking and profiling
methods or libraries. 


