---
id: gradle
title: Gradle
tags:
  - JVM
  - Kotlin
  - Benchmarking
  - Gradle
---

# Gradle Configuration

JMH can be configured to be run as a gradle task.

* [Upstream source](https://github.com/melix/jmh-gradle-plugin)


## Plugin Configuration

Outside of the `jmh` plugin a core language plugin will be needed.
This tutorial foucses on `Kotlin` but other languages can be used.

**Gradle Version Wrapper**

You can see the latest versions for the respective libraries on the upstream
source link.

**build.gradle.kts**

```kotlin
plugins {
    alias(libs.plugins.jmh)
    alias(libs.plugins.kotlin)
}
```

## Configuring JMH

The `JMH` plugin can be configured via a closure of `jmh {}`.

```kotlin
val HOME = System.getenv("HOME")

jmh {
    // Number of warmup iterations to do.
    warmupIterations.set(2)
    // Number of measurement iterations to do.
    iterations.set(2)
    // Number of subprocesses to spin up
    fork.set(2)
    // Strategy to apply when encountring duplicate classes during creation of the fat jar (i.e. while executing jmhJar task)
    duplicateClassesStrategy.set(DuplicatesStrategy.EXCLUDE)
    // Required if the jar gets rather large.
    zip64.set(true)
    // Going lower than ms is beneficial, higher is only beneficial for longer running methods.
    timeUnit.set("ms")
    // Benchmark mode. Available modes are: [Throughput/thrpt, AverageTime/avgt, SampleTime/sample, SingleShotTime/ss, All/all]
    benchmarkMode.set(mutableListOf("all"))
    // Result format to save as, json is easiest to parse.
    resultFormat.set("json")
    humanOutputFile
    // Explained in the profiler docuument.
    profilers.set(mutableListOf(
        "stack",
        "async:libPath=${HOME}/.libs/async-profiler/build/libasyncProfiler.so;output=flamegraph;dir=flamecharts"
    ))
    jmhVersion.set("1.36")
}
```

## Source Files

The `JMH` plugin is expecting source code for the benchmarks to be stored at:

`./src/jmh/main`

## Benchmarking Modes

There are several ways to benchmark and generate results, they are availalbe via `jmh.benchmarkMode`.

| Name             | Description                                                                                                                                           |
|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| Throughput       | Measures the number of operations per second, meaning the number of times per second your benchmark method could be executed.                         |
| Average Time     | Measures the average time it takes for the benchmark method to execute (a single execution).                                                          |
| Sample Time      | Measures how long time it takes for the benchmark method to execute, including max, min time etc.                                                     |
| Single Shot Time | Measures how long time a single benchmark method execution  takes to run. This is good to test how it performs under a cold start  (no JVM warm up).  |
| All              | Measures all of the above.                                                                                                                            |

