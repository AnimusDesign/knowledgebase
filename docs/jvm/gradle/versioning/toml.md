---
id: gradle-versioning-toml
title: TOML Version Wrapper
tags:
  - JVM
  - Kotlin
  - Gradle
---

Gradle offers the ability to maintain dependencies through
a `toml` configuration file. This will unify all modules 
to use the same version of a library across projects.

* [Upstream: Gradle version configuration](https://docs.gradle.org/current/userguide/platforms.html).

## Sample `TOML` File

```toml
[versions]
gradle_jmh = "0.7.0"
kotlin = "1.8.10"
kotlinx_serialization = "1.5.0"

[libraries]
kotlin = {module="org.jetbrains.kotlin:kotlin-stdlib", version.ref="kotlin"}

[plugins]
jmh = { id = "me.champeau.jmh", version.ref="gradle_jmh" }
kotlin = { id = "org.jetbrains.kotlin.jvm", version.ref="kotlin"}
```


