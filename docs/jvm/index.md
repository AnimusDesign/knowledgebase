---
sidebar_position: 1
---

# Java Virtual Machine

1. Benchmarking
  * [JMH](./benchmarking/jmh/index.md)
    * Gradle Configuration
    * Intellij Configuration
    * Profilers & Flame Charts
    * Visualization
    * Helper Scripts
  * Gatling
    * Gradle Configuration
    * API Sample
    * Custom JSON Feeder
    * Helper Scripts
  * Flight Recorder
    * Recording Data
    * Visualizing Data
    * Querying Data
    * Defining Custom Events
