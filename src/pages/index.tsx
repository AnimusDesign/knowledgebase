import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import { Content } from "@theme/BlogPostPage";

import styles from './index.module.css';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
            Docusaurus Tutorial - 5min ⏱️
          </Link>
        </div>
      </div>
    </header>
  );
}

const recentPosts = require("../../.docusaurus/docusaurus-plugin-content-blog/default/blog-post-list-prop-default.json");
export default function Home(): JSX.Element {
  console.log(recentPosts)
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title} Docs`}
      description="Documentation for Doubleshot Software">
      <main>
        <div className="row">
          <div className="col col--9 col--offset-1">
            Col 9
          </div>
        </div>
        <div className="row">
          <div className="col col--4">
            <ul>
              {recentPosts.items.slice(0, 5).map((item, index) => (
                <li key={index}>
                  <a href={`${item.permalink}`}>{item.title}</a>{" "}
                </li>
              ))}
            </ul>
          </div>
          <div className="col col--4">
            Col 4
          </div>
          <div className="col col--4">
            Col 4
          </div>
        </div>
      </main>
    </Layout>
  );
}
