---
slug: internal-performance-testing-jmh-intro
title: JMH Internal Performance Testing
authors: [animusnull]
tags: [sre, performance, ops, jvm]
---

Java micro benchmark harness, herein annotated as `JMH`.
Is a micro benchmarking tool meant for internal performance 
testing.  It looks similar to `JUnit` and a respective unit
test. But it's intent is to benchmark the performance 
of a method, generally without calling to an external service.

```kotlin
class BenchmarkLists {
  @Benchmark 
  fun benchmark_comprehension() : List<Int> {
    return (1..2).map { it  + 5 }
  }
}
```

The benchmark  annotation, is the equivalent of a `@Test` method.
It executes as a benchmark, how it runs is dependent on the configuration.

## Setup & Teardown

Like `Junit` with `@Before` there are methods to setup and configure
a benchmark run. The common use case for this is to instaniate core data
connections, load a file or some other configuration. 

As an example, in testing a serializer, rather than performing file IO
in each respective benchmark method it should be loaded before each run. 
Code called during the `@Setup` will not be considered in the final
benchmark results.

```kotlin
class BenchmarkLists {
  lateinit var rawJson :String

  @Setup
  fun setup() {
    rawJson = File.read("/tmp/data.json").readText()
  }

  @Teardown 
  fun teardown() {
    // Close file or connections.
  }

  @Benchmark 
  fun benchmark_comprehension() : List<Int> {
    return (1..2).map { it  + 5 }
  }
}
```

With kotlin you can set up properties via `lateinit var`, 
and then have them set via the `@Setup` method.

## Parameters

Each JMH benchmark has the idea of a set of parameters. These parameters
allow for benchmarking, or testing performance across several different variables.
A use case would be to test several files of varying size.



## Gradle Plugins

### Kotlinx 

* [Source](https://github.com/Kotlin/kotlinx-benchmark)

I've not had much luck with this version, and have opted for the below.

### JMH Gradle Plugin

* [Source](https://github.com/melix/jmh-gradle-plugin)


## Flame charts
